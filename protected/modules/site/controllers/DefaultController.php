<?php 
namespace app\modules\site\controllers;

use Yii;
use app\modules\site\models\Users;
use app\modules\site\models\LoginForm;
use app\components\BaseController;

/**
 * Class DefaultController
 * @package app\modules\site\controllers
 * Site controller
 */
class DefaultController extends BaseController
{
    /**
     * @return string
     * Главная страница сатйта
     */
    public function actionIndex()
	{
        $user = Users::findOne(Yii::$app->user->id);
        return $this->render('index',[
            'user' => $user
        ]);
	}

    /**
     * @return string|\yii\web\Response
     * Страница логина
     */
    public function actionLogin()
	{
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $model = new LoginForm;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
	}

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect('/');
    }
}