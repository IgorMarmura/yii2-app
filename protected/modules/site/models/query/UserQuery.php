<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 12:34
 */

namespace app\modules\site\models\query;

use yii\db\ActiveQuery;
use app\modules\site\models\Users;

class UserQuery extends ActiveQuery
{
    /**
     * Выбираем только активных пользователей
     * @internal param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function active()
    {
        $this->andWhere('status = :status', [':status' => Users::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * Выбираем только неактивных пользователей
     * @internal param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere('status_id = :status', [':status' => Users::STATUS_INACTIVE]);
        return $this;
    }

    /**
     * Выбираем только забаненных пользователей
     * @internal param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function banned()
    {
        $this->andWhere('status_id = :status', [':status' => Users::STATUS_INACTIVE]);
        return $this;
    }

    /**
     * Выбираем только простых пользователей
     * @internal param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function registered()
    {
        $this->andWhere('role_id = :role_user', [':role_user' => Users::ROLE_USER]);
        return $this;
    }
}