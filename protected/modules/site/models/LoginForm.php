<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 11:38
 */

namespace app\modules\site\models {

    use Yii;
    use yii\base\Model;


    class LoginForm extends Model
    {
        public $username;
        public $password;
        public $rememberMe = true;
        protected $_user = false;

        public function rules()
        {
            return [
                [['username', 'password'], 'required'],
                ['password', 'validatePassword'],
                ['rememberMe', 'boolean']
            ];
        }

        public function attributeLabels()
        {
            return [
                'username' => 'Логин',
                'password' => 'Пароль',
                'rememberMe' => 'Запомнить меня'
            ];
        }

        public function validatePassword()
        {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Неверный логин или пароль');
            }
        }

        public function login()
        {
            if ($this->validate()) {
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
            } else {
                return false;
            }
        }

        protected function getUser()
        {
            if ($this->_user === false) {
                $this->_user = Users::findActiveByUsername($this->username);
            }
            return $this->_user;
        }
    }
}