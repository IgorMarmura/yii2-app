<?php
namespace app\modules\site\models {

    use Yii;
    use yii\db\ActiveRecord;
    use yii\web\IdentityInterface;
    use yii\base\Security;
    use yii\rbac\DbManager;
    use yii\db\Expression;
    use yii\web\Request;

    use app\modules\admin\modules\roles\models\AuthItem;
    use app\modules\site\models\query\UserQuery;


    /**
     * @property mixed password_hash
     * @property mixed id
     * @property mixed auth_key
     */
    class Users extends ActiveRecord implements IdentityInterface
    {
        const STATUS_INACTIVE = 0;
        const STATUS_ACTIVE = 10;
        const STATUS_BANNED = 2;
        const STATUS_DELETED = 3;

        const DEFAULT_ROLE = 'User';

        public $password;
        public $status_id;
        public $avatar_url;

        /**
         * @return array
         * Добавляем дату создания и дату обновления записи пользователя
         * Добавляем ip пользователя при регистрации и при обновлении данных
         */
        public function behaviors()
        {
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\TimestampBehavior',
                    'value' => new Expression('NOW()'),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                    ],
                ],
                'ip' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'value' => $this->getUserIp(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_ip', 'updated_ip'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_ip',
                    ],
                ],
            ];
        }

        public static function tableName()
        {
            return '{{%users}}';
        }

        public function rules()
        {
            return [
                ['username', 'required'],
                ['username', 'trim'],
                ['username', 'unique'],

                ['username', 'required'],
                ['email', 'email'],
                ['email', 'trim'],
                ['email', 'unique'],

                ['password', 'required'],
                ['password', 'trim'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'username'  => 'Имя',
                'email'     => 'E-mail',
                'password'  => 'Пароль',
            ];
        }

        public function afterSave($insert)
        {
            $DbManager = new DbManager;
            $DbManager->assign($this->getDefaultRole(), $this->id);

            parent::afterSave($insert, NULL);
        }

        public function beforeSave($insert)
        {
            if (parent::beforeSave($insert)) {
                if ($this->isNewRecord) {
                    $security = new Security;
                    if (!empty($this->password)) {
                        $this->password_hash = $security->generatePasswordHash($this->password);
                    }
                    $this->auth_key = $security->generateRandomKey();
                }
                else {

                }
                return true;
            }
            return false;
        }

        /**
         * @return string
         * Получаем ip пользователя
         */
        public function getUserIp()
        {
            $request = new Request();
            return $request->getUserIP();
        }

        /**
         * @return array|null|ActiveRecord
         * Получаем дефолтную роль User для регистрации
         */
        public function getDefaultRole()
        {
            $role = AuthItem::find()
                ->where(['name' => self::DEFAULT_ROLE])
                ->one();
            return (!is_null($role)) ? $role : NULL;
        }

        public static function find()
        {
            return new UserQuery(get_called_class());
        }

        /**
         * @param int|string $id
         * @return IdentityInterface|static
         * Поиск текущего пользователя
         */
        public static function findIdentity($id)
        {
            return static::findOne($id);
        }

        /**
         * @param string $token
         * @param null $type
         * @return IdentityInterface|static
         * Поиск пользователя по токену
         */
        public static function findIdentityByAccessToken($token, $type = null)
        {
            return static::findOne(['access_token' => $token]);
        }

        /**
         * @return int|mixed|string
         * Геттер id пользователя
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return mixed|string
         * Геттер ключа авторищации
         */
        public function getAuthKey()
        {
            return $this->auth_key;
        }

        /**
         * @param string $authKey
         * @return bool
         * Валивация ключа авторизации
         */
        public function validateAuthKey($authKey)
        {
            return $this->auth_key === $auth_key;
        }

        /**
         * @param $username
         * @return array|null|ActiveRecord
         * Поиск активных пользователей по username
         */
        public static function findActiveByUsername($username)
        {
            return static::find()->where('username = :username', [':username' => $username])->active()->one();
        }

        /**
         * @param $password
         * @return bool
         * Проверка правильности пароля
         */
        public function validatePassword($password)
        {
            $security = new Security;
            return $security->validatePassword($password, $this->password_hash);
        }
    }
}