<?
    use yii\helpers\Html as html;
    use \yii\widgets\ActiveForm as form;

    $formOptions = [
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'validateOnChange' => false,
    ];
    if (isset($method) && $method) {
        $formOptions['method'] = $method;
    }
    if (isset($action) && $action) {
        $formOptions['validationUrl'] = $action;
    }
?>

<? $form = form::begin($formOptions); ?>
    <?= html::input('name','name', ['class'=>'name'])  ?>
    <?= html::submitButton('Send',['class'=>'send']) ?>
<? form::end();?>