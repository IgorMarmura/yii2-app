<?
    use yii\helpers\Html;
    use yii\helpers\Url;
    use \yii\widgets\ActiveForm;
    use yii\web\JsExpression;

    $formOptions = [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnChange' => true,
    ];
    if (isset($method) && $method) {
        $formOptions['method'] = $method;
    }
    if (isset($action) && $action) {
        $formOptions['validationUrl'] = $action;
    }
//    $this->title = Yii::t('Register', 'Регистрация');
?>

<? $form = ActiveForm::begin(); ?>

    <?= Html::activeLabel($users, 'username') ?>
    <?= Html::activeTextInput($users, 'username') ?>
    <?= Html::error($users, 'username') ?>

    <?= Html::activeLabel($users, 'email') ?>
    <?= Html::activeTextInput($users, 'email') ?>
    <?= Html::error($users, 'email') ?>

    <?= Html::activeLabel($users, 'password') ?>
    <?= Html::activeTextInput($users, 'password') ?>
    <?= Html::error($users, 'password') ?>

    <?= Html::submitButton('Register',['class'=>'btn btn-primary pull-left']) ?>

<? ActiveForm::end();?>

<script>

</script>

<h4>
    <?= $user->username ?>
</h4>