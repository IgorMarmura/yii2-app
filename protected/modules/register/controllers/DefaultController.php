<?php 
namespace app\modules\register\controllers;

use Yii;
use app\components\BaseController;
use app\modules\site\models\Users;
use yii\widgets\ActiveForm;
use yii\web\Response;

class DefaultController extends BaseController
{
	public function actionIndex()
	{
        $users = new Users();
        if($users->load($_POST, 'Users')) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($users);
            }
            if($users->save()) {
                Yii::$app->getUser()->login($users);
                $this->redirect('/');
            }
        }

		return $this->render('index',[
            'users' => $users,
		]);
	}
}