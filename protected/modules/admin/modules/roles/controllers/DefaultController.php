<?php 
namespace app\modules\admin\modules\roles\controllers;

use Yii;
use app\components\BaseController;

/**
 * Class DefaultController
 * @package app\modules\roles\controllers
 * Класс управления ролями
 */
class DefaultController extends BaseController
{
    /**
     * @return string
     * Отбражение ролей, операций, задач
     */
    public function actionIndex()
	{
        return true;
	}
}