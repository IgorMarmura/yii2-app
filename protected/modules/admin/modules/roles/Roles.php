<?php
namespace app\modules\admin\modules\roles {

    use Yii;
    use yii\base\Module;

    /**
     * Модуль управления ролям для админки [[Roles]]
     * Основной модуль frontend-приложения.
     */

    class Roles extends Module
    {

    }
}