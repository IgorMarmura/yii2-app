<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 17:45
 */

namespace app\modules\admin\modules\roles\models;

use Yii;
use yii\db\ActiveRecord;

class AuthItemChild extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%auth_item_child}}';
    }

    public function attributeLabels()
    {
        return [
            'parent'  => 'Родитель',
            'child'     => 'Ребенок',
        ];
    }
}