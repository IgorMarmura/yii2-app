<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 17:39
 */

namespace app\modules\admin\modules\roles\models;

use Yii;
use yii\db\ActiveRecord;

class Authassignment extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%auth_assignment}}';
    }

    public function attributeLabels()
    {
        return [
            'item_name'  => 'Роль',
            'user_id'     => 'Id',
            'created_at'  => 'Время создания',
        ];
    }
}