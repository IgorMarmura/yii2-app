<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 17:43
 */

namespace app\modules\admin\modules\roles\models;

use Yii;
use yii\db\ActiveRecord;

class AuthItem extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%auth_item}}';
    }

    public function rules()
    {
        return [
            ['name, type, description', 'safe'],
            ['name, type, description', 'safe', 'on' => 'search'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'  => 'Название',
            'type'     => 'Тип',
            'description'  => 'Описание',
            'created_at'  => 'Время создания',
        ];
    }
}