<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 21:35
 */

namespace app\modules\admin\modules\user\controllers {

    use app\components\BaseController;
    use app\modules\site\models\Users;
    use yii\data\ActiveDataProvider;

    class DefaultController extends BaseController
    {
        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Users::find()->active(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider
            ]);
        }
    }
}