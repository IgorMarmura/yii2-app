<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 21:37
 */

use yii\grid\GridView;

?>

<div class="row">
    <div class="col-md-12">
        <h2>Admin User</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
            echo GridView::widget([
                'id' => 'users-visits-grid',
                'dataProvider' => $dataProvider,
    //            'filterModel'  => $searchModel,

                //'options'      => ['class' => ''],
                'tableOptions' => ['class' => 'table table-striped'],
                //'rowOptions'   => ['class' => ''],

                'layout' => '{summary}<div class="panel panel-default"><div class="table-responsive">{items}</div><div class="table-footer">{pager}</div></div>',

                'columns' => [
                    'id',
                    'username',
                    'email',
                    'created_at',
                ]
            ]);
        ?>
    </div>
</div>