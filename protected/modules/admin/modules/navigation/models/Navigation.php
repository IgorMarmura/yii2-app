<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 30.06.14
 * Time: 22:16
 */

namespace app\modules\admin\modules\navigation\models {

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * @property mixed upline
     * @property mixed label
     * @property mixed type_alias
     * @property mixed parent_id
     * @property mixed tree_level
     * @property mixed is_visible
     * @property mixed target_alias
     */
    class Navigation extends ActiveRecord {

        public static function tableName()
        {
            return '{{%navigation}}';
        }
        /**
         * @return array
         */
        public function behaviors()
        {
            return [
                'ip' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'value' => '1',
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'upline',
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'upline',
                    ],
                ],
            ];
        }

        public function rules()
        {
            return [
                [['label', 'type_alias', 'upline', 'parent_id', 'tree_level', 'is_visible', 'target_alias'], 'required'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'label'  => 'Название',
                'type_alias'     => 'Тип',
                'password'  => 'Пароль',
                'is_visible'  => 'Отображение',
                'target_alias'  => 'Тип открытия ссылки',
            ];
        }



        public static function getTreeByUpline($upline)
        {
            $sql = 'SELECT * FROM navigation as nav WHERE upline LIKE CONCAT("' . $upline .'","%");';
            $tree = Navigation::findBySql($sql)->all();

            $list = [];
            foreach ($tree as $key => $value)
            {
                $list[$key] = $value->attributes;
                $list[$key]['name'] = $value->label;
            }

            foreach($list as &$node)
            {
                foreach($list as &$node_up)
                {
                    if ($node['parent_id'] === $node_up['id'])
                    {
                        if (!array_key_exists('nodes', $node_up))
                        {
                            $node_up['nodes'] = array();
                        }
                        $node_up['nodes'][] =& $node;
                        $node['parent'] =& $node_up;
                    }
                }

            }
            return $list;
        }
    }
}