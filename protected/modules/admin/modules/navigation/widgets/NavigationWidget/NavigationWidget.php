<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 30.06.14
 * Time: 21:53
 */

namespace app\modules\admin\modules\navigation\widgets\NavigationWidget {

    use Yii;
    use yii\base\Widget;
    use yii\db\Command;
    use yii\db\Connection;

    use app\modules\admin\modules\navigation\models\Navigation;

    /**
     * Class Navigation
     * @package app\modules\site\widgets\navigation
     * Виджет навигации
     */
    class NavigationWidget extends Widget {
        public $alias;
        public $view;

        public function run()
        {
            $main = Navigation::find()->where(['alias' => $this->alias])->one();
            $tree = Navigation::getTreeByUpline($main->upline);

            return $this->render($this->view, [
                'nav' => $tree[0]
            ]);
        }
    }
}