<?php
    /**
     * Created by PhpStorm.
     * User: Igor
     * Date: 01.07.14
     * Time: 0:08
     */

    use \yii\helpers\Html;
    use yii\helpers\Url;
?>

<?php if (isset($nav['nodes'])) : ?>
        <ul class="nav nav-pills nav-stacked">
            <?php foreach($nav['nodes'] as $navigation) : ?>
                <?php if((bool)$navigation['is_visible']) : ?>
                    <li>
                        <?= Html::a(Yii::t('app', $navigation['label']), Url::to($navigation['url']), ['target'=>$navigation['target_alias'], 'data-pjax'=>'pjax-container']) ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
<?php endif; ?>