<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 21:25
 */

namespace app\modules\admin\controllers;

use app\components\BaseController;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $this->checkAccess();
        return $this->render('index',[

        ]);
    }
}