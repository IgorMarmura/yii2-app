<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Nav;

/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 21:58
 */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="page-header">
                        <h1>Панель управления <small><a href="/" class="btn btn-default home">Главная</a></small></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?php
                    echo Nav::widget([
                        'options' => [
                            'class' => 'nav nav-pills nav-stacked',
                        ],
                        'items' => [
                            [
                                'label' => 'Пользователи',
                                'url' => ['/admin/user/default/index'],
                                'linkOptions' => [],
                            ],
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-10">
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>