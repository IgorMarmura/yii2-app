<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 21:21
 */

namespace app\modules\admin {

    use Yii;
    use yii\base\Module;


    class Admin extends Module
    {
        public function init()
        {
            parent::init();
            $this->layout = 'main.php';
        }
    }
}