<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'name' => 'Yii2 App',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/../web/themes/basic/views',
                    '@app/modules' => '@app/../web/themes/basic/views',
//                  '@app/widgets' => '@app/../web/themes/basic/views_widgets/widgets'
                ],
                'basePath' => '@app/../web/themes/basic/views',
                'baseUrl' => '@web/themes/basic/views',
            ],
        ],
        'user' => [
            'identityClass' => 'app\modules\site\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/default/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl'=>true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/default/index',
            ]
        ],
        'authManager'=>array(
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['User','Admin'],
        ),
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js' => ['//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js']
                ],
            ],

        ],
    ],
    'modules' => [
        'site' => [
            'class' => 'app\modules\site\Site'
        ],
        'register' => [
            'class' => 'app\modules\register\Register'
        ],
        'admin' => [
            'class' => 'app\modules\admin\Admin',
            'modules' => [
                'user' => [
                    'class' => 'app\modules\admin\modules\user\User'
                ],
                'roles' => [
                    'class' => 'app\modules\admin\modules\user\Roles'
                ],
                'navigation' => [
                    'class' => 'app\modules\admin\modules\navigation\Navigation'
                ]
            ]
        ],
    ],
    'params' => $params,
];
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment

//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = 'yii\debug\Module';
    $config['modules']['gii'] = 'yii\gii\Module';
}
return $config;