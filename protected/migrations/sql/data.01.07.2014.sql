-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.14 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица yii2.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_assignment: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('Admin', '71', NULL),
	('User', '72', 1404148107);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_item: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('Admin', 2, 'Администратор', NULL, NULL, NULL, NULL),
	('AdminDefaultIndex', 0, 'Админ панель', NULL, NULL, NULL, NULL),
	('SiteDefaultIndex', 0, 'Главная страница сайта', NULL, NULL, NULL, NULL),
	('User', 2, 'Пользователь', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_item_child: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Admin', 'AdminDefaultIndex'),
	('Admin', 'SiteDefaultIndex');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_rule: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.migration: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1397859058),
	('m130524_201442_init', 1397859063);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.navigation
CREATE TABLE IF NOT EXISTS `navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `type_alias` varchar(255) DEFAULT NULL,
  `upline` varchar(4000) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `tree_level` int(11) DEFAULT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `url` varchar(4000) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `target_alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.navigation: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `navigation` DISABLE KEYS */;
INSERT INTO `navigation` (`id`, `label`, `type_alias`, `upline`, `parent_id`, `tree_level`, `is_visible`, `url`, `alias`, `target_alias`) VALUES
	(1, 'Корень', 'catalog', '0000001', NULL, 1, 0, NULL, 'root', NULL),
	(2, 'Главное меню сайта', 'catalog', '0000001.0000002', 1, 2, 0, NULL, 'site_main', NULL),
	(3, 'Главное меню админ панели', 'catalog', '0000001.0000003', 1, 2, 0, NULL, 'admin_main', NULL),
	(4, 'Главная', 'link', '0000001.0000002.0000004', 2, 3, 1, '/', NULL, '_self'),
	(5, 'Вход', 'link', '0000001.0000002.0000005', 2, 3, 1, '/site/default/login', NULL, '_self'),
	(6, 'Регистрация', 'link', '0000001.0000002.0000006', 2, 3, 1, '/register/default/index', NULL, '_self'),
	(7, 'Выход', 'link', '0000001.0000002.0000007', 2, 3, 1, '/site/default/logout', NULL, '_self'),
	(8, 'Панель управления', 'link', '0000001.0000002.0000008', 2, 3, 1, '/admin', NULL, '_self');
/*!40000 ALTER TABLE `navigation` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.navigation_targets
CREATE TABLE IF NOT EXISTS `navigation_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.navigation_targets: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `navigation_targets` DISABLE KEYS */;
INSERT INTO `navigation_targets` (`id`, `alias`, `label`) VALUES
	(1, '_blank', 'В новое окно браузера'),
	(2, '_self', 'В текущее окно'),
	(3, '_parent', 'Во фрейм-родитель'),
	(4, '_top', 'В полное окне браузера с отменой фреймов');
/*!40000 ALTER TABLE `navigation_targets` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.navigation_types
CREATE TABLE IF NOT EXISTS `navigation_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.navigation_types: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `navigation_types` DISABLE KEYS */;
INSERT INTO `navigation_types` (`id`, `alias`, `label`) VALUES
	(1, 'link', 'Ссылка'),
	(2, 'catalog', 'Раздел');
/*!40000 ALTER TABLE `navigation_types` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_ip` varchar(255) DEFAULT NULL,
  `created_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.users: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `updated_ip`, `created_ip`) VALUES
	(71, 'admin', 'mro1odUSSahEH6uqBxfuNS4rZFKyuZYN', '$2y$13$w92lBB0RJVwIuHjD0Nqqi.YRk5s3v4Y6sKrU9GXB.Ut9nTTthwQ7y', NULL, 'admin@gmail.com', 10, '2014-06-30 20:07:03', '2014-06-30 20:07:03', '127.0.0.1', '127.0.0.1'),
	(72, 'testuser', 'z5NLLF6WE4j8OhIJzpjvPAAOiqJJNVL3', '$2y$13$B8YA.coJxl24x2GLs1sww.g67S6vXAPEH8VzNLAau3MKVFv36tH32', NULL, 'testuser@gmail.com', 10, '2014-06-30 20:08:27', '2014-06-30 20:08:27', '127.0.0.1', '127.0.0.1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
