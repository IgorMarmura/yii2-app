-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.14 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица yii2.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_assignment: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('Admin', '54', NULL),
	('User', '63', 1404065791),
	('User', '64', 1404065851),
	('User', '65', 1404065893);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_item: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('Admin', 2, 'Администратор', NULL, NULL, NULL, NULL),
	('SiteDefaultIndex', 0, 'Главная страница сайта', NULL, NULL, NULL, NULL),
	('User', 2, 'Пользователь', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_item_child: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Admin', 'SiteDefaultIndex');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.auth_rule: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.migration: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1397859058),
	('m130524_201442_init', 1397859063);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;


-- Дамп структуры для таблица yii2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2.users: ~21 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Igor', '', '', NULL, '', 10, 10, 0, 0),
	(41, 'rerewr', '', '', NULL, 'ewqeqw@wew.ewq', 10, 10, 0, 0),
	(42, 'test', '5YHjgMT0Te2pBTeWNlp1tOxatI-_VF-x', '', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(43, 'test', 'M2Ko3aZl_DSbQVFjiODf2dYNFUN8rqDT', '', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(44, 'test', 'A02Ivar5ACjQk7xglkMwvhxrk-eBeKzE', '', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(45, 'test', 'K65BoEfNIn9w5SDOVCq4KyC6iNMkAdnY', '$2y$13$vZ4Z7lgVAdQJA1nJ8mQ0LOMPQF6.3GH50tEEjCjoVjK.aAtd/DO7q', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(46, 'test', 'PmNTtsG2a1Y2yJwXcxHGlr5wiyMbZmkx', '$2y$13$gqeobZxh07UdR5OFTj4sAeQQGXFIIInKm9ZqpPPOFeNeRHpRbd2Au', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(47, 'testues', 'DZqbdK6Upw7kdkDLNnvXwMQHa_XcKv7M', '$2y$13$6NejWIPg42t4xifn30OG3eFpZI4Wy5/OwuIrV/A2IV6QiZXhGVPRe', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(48, 'testues', 'CVeCp3A4ahpG25-q5C4icncogmcJarCj', '$2y$13$nLT4XL60phUO3cxW36.AYObd3tZhWFRSupBVL8ayXFARn.1hHHYbu', NULL, 'tsee2@ds.da', 10, 10, 0, 0),
	(49, 'test', 'rRecRoPITUu6XJFhl25FiSBItNjloIIs', '$2y$13$IV6wXXXTUS8CsDla2ocbL.0NA8Aw9Xjs2QgtHTAQp0UUHUA8JdqZi', NULL, 'dwqdw@dsa.ads', 10, 10, 0, 0),
	(50, 'dawdaw', 'vGYEQyJDW1Q5kuRYda9d81K9IpxB6QCY', '$2y$13$FJcs/3M75q/W7.VDuuTt..LrPzCU.NOV1lZs4Kd.i82tk3afIc2yC', NULL, 'dwaddw@dsa.dsa', 10, 10, 0, 0),
	(51, 'dawdaw', 'THeTzjkcPNpZlfgaBTnAvIAt3z3nxV_o', '$2y$13$AcWCPvoPzUAcN2ZrSndnpe3JBB.5DOyhde70l4oPUV/rw/NgKfiQu', NULL, 'dwaddw@dsa.dsa', 10, 10, 0, 0),
	(52, 'dawdaw', 'egNr5goF0lyZ7S6zdOmHVYkBXdWSV7Lv', '$2y$13$/8FhRoUxq6vWMHE1V5rMiuCKtTMWL84AtOOr0FOYjgx9D99sss0uG', NULL, 'dwaddw@dsa.dsa', 10, 10, 0, 0),
	(53, 'dawdaw', 'xFrwd3fZTuAcB8acuM2D3H3950aED91W', '$2y$13$JIGxprjO5EUkF0PTZmKXOePsZBKus1ZZsuCRuNbaX02RDrLrt1NwC', NULL, 'dwaddw@dsa.dsa', 10, 10, 0, 0),
	(54, 'testuser', 'I8nZlXr54Cci_Loae-fgKOn3vImY4FA7', '$2y$13$qy1Tug0pDjyB3p0YQUEbn.m04tEQMQlRdjMvivsUxrbvChaTqYaba', NULL, 'testuser@gmail.com', 10, 10, 0, 0),
	(55, 'qwerty', 'nsbVdyqeGPV7QFcTc0ZmjDviuZbvor6I', '$2y$13$hK4gGm14J5A5KHUGPi.hbeKXstC7R0EICSl.IejW10jnW8LqDyqN2', NULL, 'cxzcx@xzcxzc.cxz', 10, 10, 0, 0),
	(56, 'marmura', 'rXXH5H6F3uJlC8pXPsJ9IHm4vhHbHnAL', '$2y$13$QlUKw1S99ultWrRqBWVuMuC7i4Mxp5ZWPFX0U5xvLTT6fEXd2idFu', NULL, 'igor.marmura@rambler.ru', 10, 10, 0, 0),
	(57, 'dasds', 'wOvHKfn-Ki5rmIpI7HWh0aKFuv9tPLo0', '$2y$13$eRXLrEvfgRT6Pt66MyRsu.qguuDxjdI.dwks8E/1JMieB9J4ShY56', NULL, 'adwda@dsad.da', 10, 10, 0, 0),
	(58, 'czcxcz', 'rWCoDtCCqWoPQSc5R1NnYbMRXnf6-izt', '$2y$13$YSOR7.kFGQ3wRcnJoFPzPuKkpQySIPAOVXWH6k/WP4RZnL61uUDQS', NULL, 'adwddq@dasd.das', 10, 10, 0, 0),
	(59, 'zxvxvxz', 'uHXJ02udQ4g1gKE5W2aKTvLPlkZiPkuc', '$2y$13$R0ITuTl9pz2RDaOeMh5mmesYCBdX2SX6IMtK81o0ze9Yhm4MWc4Nq', NULL, 'adwda@adwdwa.dwa', 10, 10, 0, 0),
	(60, 'zxvxvxzcxzcczx', 'S42ePvnfI5R02_Rgc5AQRqhXnxDdMHT3', '$2y$13$kxM8vifxuK41qTDana1bgOAomiCdFXX2bmZSONCKfGZEonQuUHCt.', NULL, 'adwda@adwddwwda.dwa', 10, 10, 0, 0),
	(61, 'dwdqdqq', '4D2sbv-5JvDksy445rAo50QzNGP0pQJL', '$2y$13$qcVK3JLu6MpkAJdwNA2TXugKFvwC0lrSHXWRl6Jv8SIp1Hle9NaSK', NULL, 'adwdw@dawdw.dwa', 10, 10, 0, 0),
	(62, 'qdwqdqwd', 'rbqKZzm2TZmSwg5NMITjXKHNnNh79iqD', '$2y$13$R3jx84Boyk3uwtQ9MyLEV.rAVUnhWGuUI38W1bbaa8WXtxQ2f8pHW', NULL, 'cxzcczx@dawdw.daw', 10, 10, 0, 0),
	(63, 'czxczxsdc', 'TMqKcdATq1RCyFvURkXWC2BBLnx1LdRA', '$2y$13$K8hGGIDpGyZRzQsZ9nsXHuWuZx2843cAUkec5T2tB0MepU7uN11C6', NULL, 'dwqdqd@sdads.dwadw', 10, 10, 0, 0),
	(64, 'czxczxsdc1', 'glgm734rwtrkagzTkc-hWvwX-pqLUm8d', '$2y$13$Awf8vxQ/tliJAbCk9eqwWOB2pkw13dRpVVKb3hoSjOsTx5/ffH2va', NULL, 'dwqdqd@sdads.dwadw1', 10, 10, 0, 0),
	(65, 'qwert1', 'gi3_308-OYPr0etRUty5Rzi9bqJQRzAl', '$2y$13$5HfqIui9yy.mcxRtdFzxXOFMs9sIyB2mUOg8of6kGoFVKJ6Qmn9tO', NULL, 'qwert1@qwe.qwe', 10, 10, 0, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
