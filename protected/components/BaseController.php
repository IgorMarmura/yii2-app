<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 29.06.14
 * Time: 14:17
 */

namespace app\components;

use Yii;
use yii\web\Controller;
use yii\rbac\DbManager;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class BaseController
 * @package app\components
 * Базовый контроллер проекта
 */
class BaseController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
    /**
     * @throws \yii\web\HttpException
     * Проверяем права пользователя на текущую страницу
     */
    public function checkAccess()
    {
        $DbManager = new DbManager;
        if(!$DbManager->checkAccess(Yii::$app->user->id, $this->getItem())) {
            if(Yii::$app->user->isGuest) {
                $this->redirect('/site/default/login');
            }
            else {
                throw new HttpException(403, 'Forbidden');
            }
        }
    }

    /**
     * @return string url текущей страницы вида ModuleControllerMethod
     * Конвертируем текущий url в вид ModuleControllerMethod
     */
    public function getItem()
    {
        $url = explode('/', Url::to(''));
        $item = '';
        foreach($url as $path) {
            $item = $item . ucwords($path);
        }
        return (empty($item)) ? 'SiteDefaultIndex' : $item;
    }

    /**
     * @param string $view
     * @param array $params
     * @return string вид
     * Возвращает нужный вид в зависимости типа запроса, удобная проверка для использования PJAX
     */
    public function render($view, $params = [])
    {
        return (Yii::$app->request->isAjax) ? parent::renderPartial($view, $params) : parent::render($view, $params);
    }
}