<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 27.06.14
 * Time: 0:19
 */

use yii\helpers\Html;
?>
<title><?= Html::encode($this->title) ?></title>
<meta charset="<?= Yii::$app->charset ?>"/>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="" rel="shortcut icon" type="image/x-icon" />
<?= Html::csrfMetaTags() ?>

