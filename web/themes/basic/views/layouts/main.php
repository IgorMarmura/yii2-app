<?php
    use yii\helpers\Html;
    use app\assets\AppAsset;
    use app\modules\admin\modules\navigation\widgets\NavigationWidget\NavigationWidget;
    use yii\widgets\Pjax;
    use yii\widgets\Breadcrumbs;

    /**
     * @var \yii\web\View $this
     * @var string $content
     */
    AppAsset::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

        <head>
            <?php $this->head() ?>
            <?= $this->render('_head') ?>
        </head>

        <body>
            <?php $this->beginBody() ?>
                <div class="wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page-header">
                                    <h1>Yii2 app <small> welcome</small></h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <?php
                                    echo NavigationWidget::widget([
                                            'alias' => 'site_main',
                                            'view'  => 'site_level_1'
                                        ]
                                    )
                                ?>
                            </div>
                            <?php Pjax::begin(); ?>
                                <div class="col-md-10" id="pjax-container">

                                    <div class="row" >
                                        <div class="col-md-12">
                                            <?php
                                            Breadcrumbs::widget([
                                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                            ]);
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div >
                                                <?= $content ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            <?= $this->render('_footer') ?>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>
