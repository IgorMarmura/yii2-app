<?php
    use yii\helpers\Html;
    use \yii\widgets\ActiveForm;

    $formOptions = [
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ];
    $this->params['breadcrumbs'] = [
        'label' => Yii::t('app', 'Регистрация'),
    ];
    $this->title = 'Hello world';
?>
<div class="data-title" data-title="Регистрация"></div>
<?php $form = ActiveForm::begin($formOptions); ?>

    <?= $form->field($users, 'username') .
        $form->field($users, 'email') .
        $form->field($users, 'password')->passwordInput()
    ?>

    <?= Html::submitButton('Register',['class'=>'btn btn-primary pull-left']) ?>

<?php ActiveForm::end();?>