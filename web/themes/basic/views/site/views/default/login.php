<?php
    use yii\helpers\Html;
    use \yii\widgets\ActiveForm;
    use yii\widgets\Breadcrumbs;

//    $this->params['breadcrumbs'] = [
//        'label' => Yii::t('app', 'Авторизация'),
//    ];

    $this->title = 'Авторизация';
?>

<?php
    echo Breadcrumbs::widget([
         'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
         'links' => [
             [
                 'label' => 'Авторизация',
             ],
         ],
     ]);
?>

<div class="data-title" data-title="Авторизация"></div>

<h1><?php echo Html::encode($this->title); ?></h1>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'username').
                $form->field($model, 'password')->passwordInput().
                $form->field($model, 'rememberMe')->checkbox().
                Html::submitButton('Войти', ['class' => 'btn btn-primary']);
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>