<?php
    /**
     * Created by PhpStorm.
     * User: Igor
     * Date: 29.06.14
     * Time: 14:50
     */

    use yii\helpers\Html;

    $this->params['breadcrumbs'] = [
        'label' => Yii::t('app', 'Ошибка ' . Html::encode($this->title)),
    ];
?>
<div class="site-error">
    <h1><?php echo Html::encode($this->title); ?></h1>
    <div class="alert alert-danger">
        <?php echo nl2br(Html::encode($message)); ?>
    </div>
</div>